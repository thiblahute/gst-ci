#!/bin/bash

# Script for GStreamer jenkins/repo CI

# Note: the caller is responsible to set these environment variables if required
# CC          : "ccache gcc" if ccache is installed
# CXX         : "ccache g++" if ccache is installed
# MAKEFLAGS   " "-j6" if on a quad-core machine (adjust based on machine)

# TODO: make it possible to configure what steps should be executed
# for each modules

# setup a prefix directory where everything is installed (for re-use later)
# build them one by one
# install them in the prefix
# report failures at the end
# run this from a directory that contains the checkouts for each of the
# modules

FLAVOR=$1

case "$FLAVOR" in
    fast)
	echo "Using fast variant"
	;;
    fast-build-only)
	echo "Using fast (build only) variant"
	;;
    check)
	echo "Running checks only"
	;;
    validate)
	echo "Using validate variant"
	;;
    nodebug)
	echo "Using nodebug variant"
	;;
    full)
	echo "Using full variant"
	;;
    *)
	echo "Flavor not specified or unsupported"
	echo
	echo " Usage : $0 <flavor>"
	echo
	echo "Available choices:"
	echo "  fast            : Builds without doc/introspection and runs unit tests"
	echo "  fast-build-only : Builds without doc/introspection"
	echo "  check           : Run checks only"
	echo "  nodebug         : Same as fast but with GST_DEBUG disabled"
	echo "  full            : doc/introspection build + distcheck + valgrind"
	echo "  validate        : Builds without doc/introspection and runs gst-validate"
	exit 42
	;;
esac

# modules to be built (if present)
CORE="orc gstreamer gst-plugins-base "
case "$FLAVOR" in
    validate)
	MODULES="gst-plugins-good gst-plugins-ugly gst-libav gst-plugins-bad gst-rtsp-server";;
    *)
	MODULES="gst-plugins-good gst-plugins-ugly gst-libav gst-plugins-bad gst-rtsp-server gst-editing-services gst-python";;
esac

# modules for which we run make check/distcheck
case "$FLAVOR" in
    full)
	# FIXME : distcheck fails for gst-libav
	CHECK_MODULES="orc gstreamer gst-plugins-base gst-plugins-good gst-plugins-ugly gst-plugins-bad gst-rtsp-server gst-editing-services gst-python";;
    validate)
	CHECK_MODULES="";;
    *)
	CHECK_MODULES="orc gstreamer gst-plugins-base gst-plugins-good gst-plugins-ugly gst-plugins-bad gst-libav gst-rtsp-server gst-editing-services gst-python";;
esac

# FIXME : Gradually add more modules
VALGRIND_MODULES=""

ERROR_RETURN=255

echo
echo "======================================================"
echo

if [ -z $VALIDATE_HTTP_PORT ];
then
    echo "Using default validate HTTP port: 8039"
    VALIDATE_HTTP_PORT=8039
else
    echo "Using specified validate HTTP port:" $VALIDATE_HTTP_PORT
fi

if [ -z $VALIDATE_MODULES ];
then
    echo "Using default validate modules: validate ges"
    VALIDATE_MODULES="validate ges"
else
    echo "Using specified validate modules:" $VALIDATE_MODULES
fi

if [ -z $WORKSPACE ];
then
    echo "Out-of-jenkins build"
    if [ -d $PWD/gstreamer ];
    then
	echo "Using current directory as workspace"
    else
	echo "This script must be run from the directory containing the various modules to build"
	exit
    fi
    WORKSPACE=$PWD
    BUILD_TAG="local"
else
    echo "Jenkins build"
fi
echo

# Create an output directory for all results
#
# output/
#   prefix/      # symbolic link to $BUILD_TAG/prefix (if flavor full)
#   $BUILD_TAG/
#      logs/     # Where logs from the various steps are stored
#      prefix/   # Install prefix (if flavor:full)
#      registry.dat  # Llocation of the registry (if flavor full)

OUTPUTDIR=$WORKSPACE/output/$BUILD_TAG

LOGDIR=$OUTPUTDIR/logs
mkdir -p $LOGDIR

echo
echo "======================================================"
echo
echo "Modules scheduled to be built:"
echo "  $CORE $MODULES"
echo
echo "Modules scheduled to be checked (make check):"
echo "  $CHECK_MODULES"
echo

if test "x$FLAVOR" == "xfull"; then
    echo "Modules scheduled to be checked for leaks (make check-valgrind):"
    echo "  $VALGRIND_MODULES"
    echo

    # # prefix directory
    # INSTALLPREFIX=$WORKSPACE/output/prefix

    # mkdir -p $OUTPUTDIR/prefix
    # ln -sf $OUTPUTDIR/prefix $INSTALLPREFIX

    # # setup the various env variables
    # export PATH="$INSTALLPREFIX/bin":$PATH
    # export LD_LIBRARY_PATH="$INSTALLPREFIX/lib":$LD_LIBRARY_PATH
    # export DYLD_LIBRARY_PATH="$INSTALLPREFIX/lib":$DYLD_LIBRARY_PATH
    # export GI_TYPELIB_PATH="$INSTALLPREFIX/share/gir-1.0":$GI_TYPELIB_PATH
    # export PKG_CONFIG_PATH="$INSTALLPREFIX/lib/pkgconfig/":$PKG_CONFIG_PATH
    # export GST_REGISTRY="$INSTALLPREFIX/gstreamer-registry.dat"

    # Checks are disabled for full variants
    export GST_CHECKS="nothing-at-all-go-away-die-die"
fi

echo "======================================================"
echo
echo "Grabbing Environment variables  => output/$BUILD_TAG/env.log"
echo

env > $LOGDIR/env.log 2>&1

echo

# build the specified module
build()
{
    if test -d $WORKSPACE/$1; then
	echo "Building $1"

	cd $WORKSPACE/$1

	# If needed, re-run autogen
	if test ! -e Makefile
	then
	    if test -e autoregen.sh
	    then
		echo "+ $1: autoregen.sh"
		logname=$LOGDIR/$1-autoregen.log
		./autoregen.sh > $logname 2>&1
		if test $? -ne 0
		then
		    echo "AUTOREGEN FAILURE"
		    cat $logname
		    exit $ERROR_RETURN
		fi
	    else
		echo "+ $1: autogen.sh"
		logname=$LOGDIR/$1-autogen.log
		# Fast variant => no docs
		# Full variant => introspection and docs
		case "$FLAVOR" in
		    fast*)
			AGARGS="--disable-gtk-doc --disable-docbook --enable-introspection";;
		    validate)
			AGARGS="--disable-gtk-doc --disable-docbook --enable-introspection --disable-examples --disable-tests --disable-check";;
		    nodebug)
			AGARGS="--disable-gst-debug --disable-gtk-doc --disable-docbook --enable-introspection";;
		    full)
			AGARGS="--enable-gtk-doc --enable-docbook --enable-introspection";;
		    *)
			AGARGS="";;
		esac
		./autogen.sh $AGARGS > $logname 2>&1

		if test $? -ne 0
		then
		    echo "AUTOGEN FAILURE"
		    cat $logname
		    exit $ERROR_RETURN
		fi
	    fi
	else
	    echo "+ $1: git submodule update"
	    git submodule update 2>&1 > /dev/null
	fi

	echo "+ $1 : make"

	logname=$LOGDIR/$1-make.log
	make V=1 > $logname 2>&1
	if test $? -ne 0
	then
	    echo "MAKE FAILURE"
	    cat $logname
	    exit $ERROR_RETURN
	fi

	case "$FLAVOR" in
	    validate)
		echo "+ $1 : Skipping build-checks";;
	    *)
		if test $1 != "orc" -a $1 != "gst-devtools/validate" -a $1 != "gst-python"
		then
		    echo "+ $1 : make build-checks"
		    logname=$LOGDIR/$1-build-checks.log
		    make build-checks V=1 > $logname 2>&1
		    if test $? -ne 0
		    then
			echo "MAKE BUILD-CHECKS FAILURE"
			cat $logname
			exit $ERROR_RETURN
		    fi
		fi
	esac

	# if test "x$FLAVOR" == "xfull"; then
	#     echo "+ $1 : make install"
	#     logname=$LOGDIR/$1-make-install.log
	#     make install V=1 > $logname 2>&1
	#     if test $? -ne 0
	#     then
	# 	echo "MAKE INSTALL FAILURE"
	# 	cat $logname
	# 	exit $ERROR_RETURN
	#     fi
	# fi

	echo "+ $1 : make clean-cruft"
	make clean-cruft > /dev/null 2>&1
    fi
}

make_distcheck()
{
    if test -d $WORKSPACE/$1; then
	cd $WORKSPACE/$1

	echo "+ $1 : make distcheck"
	logname=$LOGDIR/$1-make-distcheck.log
	GST_CHECKS=none make distcheck V=1 > $logname 2>&1
	
	if test $? -ne 0
	then
	    echo "MAKE DISTCHECK FAILURE"
	    cat $logname
	    exit $ERROR_RETURN
	fi
    fi
}

make_check()
{
    if test -d $WORKSPACE/$1; then
	cd $WORKSPACE/$1

	echo "+ $1 : make check"
	logname=$LOGDIR/$1-make-check.log
	# remove pending xml check files
	rm -f tests/check/*/*.xml

	GST_CHECK_XML=1 make check V=1 > $logname 2>&1

	if test $? -ne 0
	then
	    echo "+ $1 : MAKE CHECK FAILURE"
	    cat $logname
	    # We don't want to error out except for gst-python
	    if test $1 == "gst-python"
	       then
		   exit $ERROR_RETURN
	    fi
	fi
    fi
}

valgrind()
{
    if test -d $WORKSPACE/$1; then
	cd $WORKSPACE/$1

	echo "+ $1 : make check-valgrind"
	logname=$LOGDIR/$1-make-check-valgrind.log
	make check-valgrind V=1 > $logname 2>&1

	if test $? -ne 0
	then
	    echo "MAKE CHECK-valgrind FAILURE"
	    cat $logname
	    exit $ERROR_RETURN
	fi
    fi
}

beach()
{
    echo "======================================================"
    echo
    echo "Checking available elements"
    echo

    gst-inspect-1.0 | tail -n1

    echo
    echo "Checking blacklisted elements"

    gst-inspect-1.0 -b

    if test "x$FLAVOR" == "xvalidate"; then
	echo
	echo "======================================================"
	echo
	mkdir -p $LOGDIR/gst-devtools
	build "gst-devtools/validate"
	cd $WORKSPACE
	build "gst-editing-services"
	cd $WORKSPACE
	mkdir -p "validate-output"
	gst-validate-launcher $VALIDATE_MODULES --shuffle -fs -m -n  --check-bugs --fail-on-testlist-change -M $WORKSPACE/validate-output --xunit-file $WORKSPACE/xunit.xml --http-server-port $VALIDATE_HTTP_PORT
    fi

    echo
    echo "======================================================"
    echo
    echo "Cleaning up build directories"

    # ccache will have cached all non-changed compilation, we can safely make clean
    repo forall -c make clean > /dev/null 2>&1

    # if test "x$FLAVOR" == "xfull"; then
    # 	echo
    # 	echo "======================================================"
    # 	echo
    # 	echo "Compressing/storing output prefix"
    # 	cd $OUTPUTDIR
    # 	tar cvJf $BUILD_TAG.tar.xz prefix > /dev/null
    # 	ls -lah $BUILD_TAG.tar.xz
    # 	rm -Rf prefix
    # 	echo
    # 	echo "======================================================"
    # 	rm $INSTALLPREFIX
    # fi

    echo "Update done"
    exit
}


# build selected modules
for m in $CORE $MODULES; do
    case "$FLAVOR" in
	check)
	;;
	*)
	    build $m
	    ;;
    esac
done

# make check (for those activated)
for m in $CHECK_MODULES; do
    case "$FLAVOR" in
	full)
	    make_distcheck $m
	    ;;
	fast-build-only)
	    ;;
	*)
	    make_check $m
	    ;;
    esac
done

# make valgrind (for those activated)
for m in $VALGRIND_MODULES; do
    cd $WORKSPACE/$m
done

case "$FLAVOR" in
    fast-build-only)
	echo "Not cleaning up"
	;;
    *)
	beach
	;;
esac
